const movies = [{
  "id": 1,
  "gender": "Horror|Thriller",
  "title": "Halloween II",
  "year": 2009,
  "synopsis": "architect enterprise bandwidth"
}, {
  "id": 2,
  "gender": "Drama",
  "title": "Bad and the Beautiful, The",
  "year": 1989,
  "synopsis": "deploy 24/7 web services"
}, {
  "id": 3,
  "gender": "Western",
  "title": "Run for Cover",
  "year": 2000,
  "synopsis": "enhance sexy e-business"
}, {
  "id": 4,
  "gender": "Action|Thriller",
  "title": "Paintball",
  "year": 1994,
  "synopsis": "enable killer partnerships"
}, {
  "id": 5,
  "gender": "Comedy",
  "title": "Nothing to Declare (Rien à déclarer)",
  "year": 2004,
  "synopsis": "mesh bleeding-edge models"
}, {
  "id": 6,
  "gender": "Mystery|Thriller",
  "title": "Obsession",
  "year": 2006,
  "synopsis": "mesh 24/365 relationships"
}, {
  "id": 7,
  "gender": "Action|Comedy|Drama|Musical|Romance",
  "title": "Dabangg 2",
  "year": 2001,
  "synopsis": "benchmark wireless paradigms"
}, {
  "id": 8,
  "gender": "Action|Children|Drama",
  "title": "Karate Kid, The",
  "year": 1999,
  "synopsis": "leverage robust ROI"
}, {
  "id": 9,
  "gender": "Crime|Mystery|War",
  "title": "Sherlock Holmes and the Voice of Terror",
  "year": 1990,
  "synopsis": "mesh global methodologies"
}, {
  "id": 10,
  "gender": "Documentary",
  "title": "Gonzo: The Life and Work of Dr. Hunter S. Thompson",
  "year": 2001,
  "synopsis": "harness strategic technologies"
}];

module.exports = {
  movies
}