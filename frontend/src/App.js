import React from 'react'
import './App.css'
import NavBar from './components/NavBar/NavAppBar'
import Home from './pages/Home'
import Movies from './pages/Movies'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

function App () {
  return (
    <div className='App'>
      <Router>
        <NavBar />
        <Switch>
          <Route path='/' exact>
            <Home />
          </Route>
          <Route path='/movies'>
            <Movies />
          </Route>
        </Switch>
      </Router>
    </div>
  )
}

export default App
