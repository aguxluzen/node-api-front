import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import MovieCard from '../components/MovieCard'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'

const useStyles = makeStyles({
  root: {
    paddingTop: 16
  }
})

const Movies = () => {
  const classes = useStyles()

  return (
    <>
      <Container>
        <Grid container spacing={2} className={classes.root}>
          <Grid item xs={12}>
            <Grid container justify='flex-start' spacing={4}>
              {
                // TODO obtener listado de peliculas desde la api y mostrar
              }
              <Grid item>
                <MovieCard />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}

export default Movies
