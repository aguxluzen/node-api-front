import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'

import TypeCard from '../components/TypeCard'

const useStyles = makeStyles({
  root: {
    paddingTop: 16
  }
})

const Home = () => {
  const classes = useStyles()
  
  
  return (
    <>
      <Container className={classes.root}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Grid container justify='flex-start' spacing={4}>
              {
                // TODO obtener listado de peliculas desde la api y mostrar
              }
              <Grid item>
                <TypeCard />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </>
  )
}

export default Home
