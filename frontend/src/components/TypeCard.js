import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles({
  root: {
    minWidth: 200,
    minHeight: 150
  },
  pos: {
    marginTop: 0
  }
})

export default function TypeCard () {
  const classes = useStyles()

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant='h5' className={classes.pos} component='h2'>
          Series
        </Typography>
      </CardContent>
    </Card>
  )
}
